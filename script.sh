#!/bin/bash
apt-get update && apt-get upgrade -y
apt-get install -y dos2unix && \
    rm -rf /var/lib/apt/lists/*
chmod +x ./src/test/resources/chromedriver_linux
dos2unix /app/script.sh && chmod +x /app/script.sh
mvn clean
mvn test
